
/datum/table_recipe
	var/name = "" //in-game display name
	var/reqs[] = list() //type paths of items consumed associated with how many are needed
	var/result //type path of item resulting from this craft
	var/tools[] = list() //type paths of items needed but not consumed
	var/time = 30 //time in deciseconds
	var/parts[] = list() //type paths of items that will be placed in the result
	var/chem_catalysts[] = list() //like tools but for reagents
	var/category = CAT_NONE //where it shows up in the crafting UI


/datum/table_recipe/gg
	name = "Combination of two green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/gg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 2)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/gb
	name = "Combination of green and blue herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/gb
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 1)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/rg
	name = "Combination of red and green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/rg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 1)
	time = 20
	category = CAT_MEDICAL

/datum/table_recipe/ggg
	name = "Combination of three green herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/ggg
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 3)
	time = 40
	category = CAT_MEDICAL

/datum/table_recipe/rgb
	name = "Combination of red, green and blue herbs"
	result = /obj/item/weapon/reagent_containers/pill/stalker/trava/rgb
	reqs = list(/obj/item/weapon/reagent_containers/pill/stalker/trava/green = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/red = 1,
	/obj/item/weapon/reagent_containers/pill/stalker/trava/blue = 1)
	time = 40
	category = CAT_MEDICAL


/datum/table_recipe/pammosmall
	name = "15x pistol ammo"
	result = /obj/item/ammo_box/stalker/bmag44
	reqs = list(/obj/item/weapon/metro/loot/craft/bluepowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/pammostandart
	name = "30x pistol ammo"
	result = /obj/item/weapon/storage/box/metro/pistol_44_mag_small
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 1,
	/obj/item/weapon/metro/loot/craft/bluepowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/pammobig
	name = "60x pistol ammo"
	result = /obj/item/weapon/storage/box/metro/pistol_44_mag
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammosmall
	name = "8x shotgun ammo"
	result = /obj/item/ammo_box/stalker/b12x70
	reqs = list(/obj/item/weapon/metro/loot/craft/bluepowder = 1,
	/obj/item/weapon/metro/loot/craft/yellowpowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammostandart
	name = "16x shotgun ammo"
	result = /obj/item/weapon/storage/box/metro/shotgun_b12x70_small
	reqs = list(/obj/item/weapon/metro/loot/craft/redpowder = 1,
	/obj/item/weapon/metro/loot/craft/yellowpowder = 1)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/sammobig
	name = "40x shotgun ammo"
	result = /obj/item/weapon/storage/box/metro/shotgun_b12x70
	reqs = list(/obj/item/weapon/metro/loot/craft/yellowpowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/rammo
	name = "36x rifle ammo"
	result = /obj/item/weapon/storage/box/metro/rifle_545x39_small
	reqs = list(/obj/item/weapon/metro/loot/craft/whitepowder = 2)
	time = 30
	category = CAT_AMMO

/datum/table_recipe/bint
	name = "Guaze"
	result = /obj/item/stack/medical/bintik
	reqs = list(/obj/item/weapon/metro/loot/craft/spirt = 1,
	/obj/item/weapon/metro/loot/craft/tryapka = 1)
	time = 25
	category = CAT_MEDICAL

/datum/table_recipe/metro/flare
	name = "Flare"
	result = /obj/item/device/flashlight/flare
	reqs = list(/obj/item/weapon/metro/loot/craft/metall = 1,
	/obj/item/weapon/metro/loot/craft/powder = 3)
	time = 30
	category = CAT_MISC