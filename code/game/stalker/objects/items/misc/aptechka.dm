/obj/item/weapon/reagent_containers/pill/stalker
	icon = 'icons/stalker/items.dmi'
	w_class = 2
	possible_transfer_amounts = list()
	volume = 60
	apply_type = PATCH
	apply_method = "apply"

/obj/item/weapon/reagent_containers/pill/stalker/aptechka
	name = "kit"
	desc = "�����&#255; ��������&#255; �������."

obj/item/weapon/reagent_containers/pill/stalker/afterattack(obj/target, mob/user , proximity)
	return // thanks inheritance again

obj/item/weapon/reagent_containers/pill/stalker/canconsume(mob/eater, mob/user)
	if(!iscarbon(eater))
		return 0
	return 1 // Masks were stopping people from "eating" patches. Thanks, inheritance.

/obj/item/weapon/reagent_containers/pill/stalker/spray
	name = "spray can"
	icon = 'icons/stalker/sPRAY.dmi'
	icon_state = "spray"
	desc = "������&#255; ����� ������ �����������, ������� �����-�� ������� ���������� � ������ ����������."
	eng_desc = "Green spray."
	item_state = "spray"
	list_reagents = list("cryoxadoney" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus
	name = "morphine (+)"
	icon = 'icons/Unsorted/Pack11.dmi'
	icon_state = "14"
	desc = "����������������&#255; ������ ���� ������ �������������� � ����� ������. ��&#255; �������� ������� - ���������&#255; ������&#255;�� ��� ��������� �����."
	eng_desc = "Experimental reagent."
	item_state = "pillpain"
	list_reagents = list("cryoxadone" = 8)

/obj/item/weapon/reagent_containers/pill/stalker/trava/green
	name = "Green herbage"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "greenflower"
	desc = "������������ ����&#255;�. ����� �� ����� �����������&#255; ��� �������� ��������� ����������"
	eng_desc = "Strange flower"
	item_state = "greenflower"
	list_reagents = list("cryoxadone" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/trava/red
	name = "Red herbage"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "pinkflower"
	desc = "������������ ����&#255;�. ����� �� ����� �����������&#255; ��� �������� ��������� ����������"
	eng_desc = "Strange flower"
	item_state = "pinkflower"
	list_reagents = list("cryoxadone" = 5)

/obj/item/weapon/reagent_containers/pill/stalker/trava/blue
	name = "Blue herbage"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "purpleflower"
	desc = "������������ ����&#255;�. ����� �� ����� �����������&#255; ��� �������� ��������� ����������"
	eng_desc = "Strange flower"
	item_state = "purpleflower"
	list_reagents = list("pen_acid" = 9, "charcoal" = 30)

/obj/item/weapon/reagent_containers/pill/stalker/trava/gg
	name = "G+G"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "gg"
	desc = "���������&#255; ���� ������ ����."
	eng_desc = "Combination of two green herbs."
	item_state = "gg"
	list_reagents = list("cryoxadone" = 16)

/obj/item/weapon/reagent_containers/pill/stalker/trava/rg
	name = "G+R"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "rg"
	desc = "���������&#255; ������ � ������� �����."
	eng_desc = "Combination of red and green herbs."
	item_state = "rg"
	list_reagents = list("cryoxadoneb" = 8)

/obj/item/weapon/reagent_containers/pill/stalker/trava/rgb
	name = "R+G+B"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "rgb"
	desc = "���������&#255; ������, ������� � ����� �����."
	eng_desc = "Combination of red, green and blue herbs."
	item_state = "rgb"
	list_reagents = list("cryoxadoney" = 6)

/obj/item/weapon/reagent_containers/pill/stalker/trava/gb
	name = "G+B"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "gb"
	desc = "���������&#255; ������ � ����� �����."
	eng_desc = "Combination of green and blue herbs."
	item_state = "gb"
	list_reagents = list("cryoxadone" = 10, "pen_acid" = 18)

/obj/item/weapon/reagent_containers/pill/stalker/trava/ggg
	name = "G+G+G"
	icon = 'icons/stalker/drugs.dmi'
	icon_state = "ggg"
	desc = "���������&#255; ��� ������ ����."
	eng_desc = "Combination of three green herbs."
	item_state = "ggg"
	list_reagents = list("cryoxadoney" = 6)