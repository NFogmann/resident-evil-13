/obj/item/documents
	name = "secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper."
	icon = 'icons/obj/bureaucracy.dmi'
	icon_state = "docs_generic"
	item_state = "paper"
	throwforce = 0
	w_class = 1
	throw_range = 1
	throw_speed = 1
	layer = 4
	pressure_resistance = 1

/obj/item/documents/nanotrasen
	desc = "\"Top Secret\" Nanotrasen documents printed on special copy-protected paper. It is filled with complex diagrams and lists of names, dates and coordinates."
	icon_state = "docs_verified"

/obj/item/documents/syndicate
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence."

/obj/item/documents/syndicate/red
	name = "'Red' secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence. These documents are marked \"Red\"."
	icon_state = "docs_red"

/obj/item/documents/syndicate/blue
	name = "'Blue' secret documents"
	desc = "\"Top Secret\" documents printed on special copy-protected paper. It details sensitive Syndicate operational intelligence. These documents are marked \"Blue\"."
	icon_state = "docs_blue"

/obj/item/documents/resident
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_1"
	desc = "�����&#255; ����� � �����������. �� ��������� ����� ��������: \"�������� ����������� �������&#255; ��������� � ��������\", ������ - \"K-04\". ����� ���� ����� �����-�� ������, ����������� � �������� ����������. ������ ���������� ������ ���."

/obj/item/documents/resident/lvl2
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_2"
	desc = "����� ������ � ������ \"������ ��������\". �������� ����������, � ��&#255;�� � ���������� �������� ����������, ��� ����� ������ �� �������� ��������� \"T-17\" �� 08.06.15. ���� �������� ��&#255;��� ���� ��������������� � \"��������� �����������\" ������ �� ����� ��������� � ������� �� �����������&#255;� � ������� 3� ����. ������ ��������� ����� ������ ���� ������� ��� ������ � ����� ���� ������ �� �������� �� ������ 141 �� ���������� ��������� ����."

/obj/item/documents/resident/lvl3
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "documents_3"
	desc = "����� ������ � ������ \"���������� ��������\". � ���������� ��������&#255; �� ���������� �������� �����������&#255; � �������������� �������� � ����� ����������� \"K-3\" �� ���� ������� \"��� �������\". �������� ������� �61 �� 27.01.15 � ����������� ��� ���������� ������� ���������� �������� � �������� ������������, �� ���� ������ �����&#255;���&#255;  4 \"������ �������\" � 2 \"������� ��&#255; ��������������� �������&#255;\"."
