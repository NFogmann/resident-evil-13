/obj/item/weapon/card/id/metro
	name = "civilian identification card"
	desc = "�����-�������� �������, ������������ � ����� ����������� �����������. �� ��� ����� ��������&#255; ����� � ������ ��������. �� ���� ��������� - ��� ����������� � ����� �� ����������� �������."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "documents"
	item_state = "card-id"
	registered_name = "Civilian"
	slot_flags = SLOT_ID
	assignment = "Civilian"

/obj/item/weapon/card/id/metro/trc
	name = "D.E.S.T.R.O. employee identification card"
	desc = "�����-�������� �������."
	icon = 'icons/stalker/lohweb/items.dmi'
	icon_state = "pdocuments"
	item_state = "card-id"
	registered_name = "TRC dweller"
	slot_flags = SLOT_ID
	assignment = "Employee"

/obj/item/weapon/card/id/metro/stalker
	name = "ERT dogtag"
	desc = "��������� ������������� �����."
	icon = 'icons/Unsorted/mistakes (1).dmi'
	icon_state = "dogtag"
	item_state = "card-id"
	registered_name = "ERT Operative"
	slot_flags = SLOT_ID
	assignment = "ERT Operative"

/obj/item/weapon/card/id/metro/key
	name = "gold key"
	desc = "��������� ������� ������."
	icon = 'icons/stalker/some_stuff/items.dmi'
	icon_state = "key3"
	item_state = "card-id"
	registered_name = null
	slot_flags = SLOT_ID
	assignment = null

/obj/item/weapon/card/id/metro/idblue
	name = "blue id-card"
	desc = "���&#255;&#255; ����-�����."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_blue"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_security)

/obj/item/weapon/card/id/metro/idyellow
	name = "yellow id-card"
	desc = "Ƹ���&#255; ����-�����."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_yellow"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_emergency_storage)

/obj/item/weapon/card/id/metro/idred
	name = "red id-card"
	desc = "������&#255; ����-�����."
	icon = 'icons/stalker/some_stuff/quest.dmi'
	icon_state = "id_card_red"
	item_state = "card-id"
	slot_flags = SLOT_ID
	access = list(access_research)